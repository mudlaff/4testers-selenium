from selenium.webdriver.common.by import By


class NewProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def add_project_name(self, name):
        name_input = self.browser.find_element(By.CSS_SELECTOR, '#name')
        name_input.send_keys(name)

    def add_project_prefix(self, prefix):
        prefix_input = self.browser.find_element(By.CSS_SELECTOR, '#prefix')
        prefix_input.send_keys(prefix)

    def add_project_description(self, description):
        description_input = self.browser.find_element(By.CSS_SELECTOR, '#description')
        description_input.send_keys(description)

    def save_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()
