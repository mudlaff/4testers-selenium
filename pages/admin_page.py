from selenium.webdriver.common.by import By


class AdminPage:
    def __init__(self, browser):
        self.browser = browser

    def click_new_project_button(self):
        new_project_button = self.browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT')
        new_project_button.click()

    def use_search_bar(self, name):
        search_bar = self.browser.find_element(By.CSS_SELECTOR, '#search')
        search_bar.send_keys(name)
        search_button = self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton')
        search_button.click()
