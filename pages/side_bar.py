from selenium.webdriver.common.by import By


class SideBar:
    def __init__(self, browser):
        self.browser = browser

    def go_to_project_list(self):
        project_list = self.browser.find_element(By.CSS_SELECTOR, '.activeMenu')
        project_list.click()
