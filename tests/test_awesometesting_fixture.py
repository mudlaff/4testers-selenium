import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture()
def browser():
    # Faza 1  - wykonywane przed testem
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.get('https://www.awesome-testing.com')

    # faza 2 to co przekazujemy do kazdego testu
    yield browser

    # faza 3 - po wykonaniu testu
    browser.quit()


def test_post_count(browser):
    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 4 elementy
    assert len(titles) == 4


def test_post_count_after_search(browser):
    # Inicjalizacja searchbara i przycisku search
    search_box = browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button')

    # Szukanie
    search_box.send_keys("cypress")
    search_button.click()

    # Czekanie na stronę

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 3 elementy
    assert len(titles) == 5


def test_post_count_on_cypress_label(browser):
    # Inicjalizacja elementu z labelką
    label = browser.find_element(By.LINK_TEXT, 'Cypress')

    # Kliknięcie na labelkę
    label.click()

    # Czekanie na stronę

    # Pobranie listy tytułów
    titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 1 element
    assert len(titles) == 1
