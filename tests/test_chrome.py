import time

from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver

def test_example():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get("https://www.bing.com/")
    assert "Bing" in driver.title
    driver.fullscreen_window()   # fullscreen tak jakby film oglądać
    time.sleep(2)
    driver.maximize_window()  # okno rozszerzone na cały ekran. Bezpieczniejsza opcja
    time.sleep(2)
    driver.set_window_size(1280, 720)  # ustawienie konkretnego rozmiaru okna
    time.sleep(2)
    driver.quit()
