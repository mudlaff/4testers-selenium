import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pages.admin_page import AdminPage
from pages.cockpit_page import CockpitPage
from pages.login_page import LoginPage
from pages.new_project_page import NewProjectPage
from pages.side_bar import SideBar
from random_data.generate_random_data import RandomUtil


@pytest.fixture()
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    login_page = LoginPage(browser)
    login_page.load()
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield browser
    browser.quit()


def test_new_project_creation(browser):
    assert 'TestArena Demo' in browser.title

    cockpit_page = CockpitPage(browser)
    cockpit_page.click_administration()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

    admin_page = AdminPage(browser)
    admin_page.click_new_project_button()
    assert 'Dodaj projekt - TestArena Demo' in browser.title

    name = RandomUtil.get_random_string(10)
    prefix = RandomUtil.get_random_string(6)
    description = RandomUtil.get_random_string(50)

    new_project_page = NewProjectPage(browser)
    new_project_page.add_project_name(name)
    new_project_page.add_project_prefix(prefix)
    new_project_page.add_project_description(description)

    new_project_page.save_new_project()
    assert browser.find_element(By.CSS_SELECTOR, '.content_label_title').text == name

    project_list = SideBar(browser)
    project_list.go_to_project_list()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

    admin_page.use_search_bar(name)
    search_results = browser.find_elements(By.CSS_SELECTOR, 'table > tbody')
    assert len(search_results) == 1
