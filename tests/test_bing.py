import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://www.bing.com/')

    # Znalezienie paska wyszukiwania
    search_box = browser.find_element(By.CSS_SELECTOR, '#sb_form_q')

    # Znalezienie guzika wyszukiwania (lupki)
    magnifier_icon = browser.find_element(By.CSS_SELECTOR, '#search_icon')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_box.is_displayed() is True
    assert magnifier_icon.is_displayed() is True

    # Szukanie Vistula University
    search_box.send_keys('Vistula University')
    magnifier_icon.click()

    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, '.b_algo')
    assert len(search_results) > 2

    # Sprawdzenie że pierwszy wynik ma tytuł 'Vistula University in Warsaw'
    first_result = browser.find_element(By.CSS_SELECTOR, '.b_topTitle')
    text_inside_element = first_result.text
    assert text_inside_element == 'Home - Vistula University'

    # Zamknięcie przeglądarki
    time.sleep(2)
    browser.close()
